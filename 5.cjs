const arr = require('./1-arrays-jobs.cjs');
sumCountry(arr);

function sumCountry(arr){

    const result = arr.reduce((acc,data)=>{
        if(acc[data.location]){
            acc[data.location]+= parseFloat(data.salary.slice(1));
        }else{
            acc[data.location] = parseFloat(data.salary.slice(1));
        }
        return acc;
    },{})
    console.log(result);
}